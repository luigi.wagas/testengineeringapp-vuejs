describe("Unique checkins list", () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should be able to view previous checkins list', function () {
    addUser("John Doe")
    addUser("Tony Stark")
    addUser("Steve Rogers")

    cy.contains('Guests').click()

    cy.get('ul > li')
      .should('have.length', 3)

    validateUserOnList(0, "John Doe")
    validateUserOnList(1, "Tony Stark")
    validateUserOnList(2, "Steve Rogers")
  });

  it('should not contain duplicate user names', function () {
    addUser("John Doe")
    addUser("Tony Stark")
    addUser("john doe") // duplicate

    cy.contains('Guests').click()

    cy.get('ul > li')
      .should('have.length', 2)
  });

  const addUser = (name: string) => {
    cy.get('input').type(name)
    cy.contains('Next').click()
    cy.contains('Back').click()
  }

  const validateUserOnList = (index: number, name: string) => {
    cy.get('ul > li').eq(index).should('have.text', name)
  }
})