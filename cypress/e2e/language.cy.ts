const NAME = "John Doe"

describe("Change language setting", () => {
  beforeEach(() => {
    cy.visit('/')
    cy.get('input').type(NAME)
    cy.contains('Next').click()
  })

  it('should be able to toggle language ', function () {
    // Should be english by default
    cy.get('.menu').should('have.text', 'English')

    cy.get('.menu').click().should('have.text', 'Tagalog')
    cy.get('.menu').click().should('have.text', 'English')
  });

  it('should translate the greeting when language setting is changed', function () {
    cy.get('.menu').click() // Tagalog
    cy.get('.greeting').should('contain.text', `Kamusta ${NAME}`)

    cy.get('.menu').click() // English
    cy.get('.greeting').should('contain.text', `Hello ${NAME}`)
  });
})