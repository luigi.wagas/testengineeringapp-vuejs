const NAME = "John Doe"

describe("Prompt for user's name", () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('should ask user for their name', function () {
    cy.get("div").should("contain.text", "What's your name?")
    cy.get("input").should('exist')
  });

  it("should display welcome message with user name after clicking next", function () {
    checkin(NAME);
    cy.get('.greeting').should("contain.text", `Hello ${NAME}`)
  });

  it('name should not be empty', function () {
    cy.get("input").should('not.have.text')
    cy.contains("Next").click()
    cy.location('pathname').should('eq', '/')
  });

  function checkin(name: string) {
    cy.get("input").type(name)
    cy.contains("Next").click()
  }
})