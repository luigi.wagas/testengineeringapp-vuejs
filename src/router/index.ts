import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/greet/:name',
      name: 'greet',
      component: () => import('../views/GreetingView.vue')
    },
    {
      path: '/guests',
      name: 'guests',
      component: () => import('../views/GuestsView.vue')
    },
  ]
})

export default router
