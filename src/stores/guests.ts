import { ref, computed, reactive } from 'vue'
import { defineStore } from 'pinia'

export const useStore = defineStore('store', () => {
  const Languages = {
    "english": {
      "label": "English",
      "code": "en",

      "strings": {
        "prompt": "What's your name?",
        "greeting": "Hello"
      }
    },
    "tagalog": {
      "label": "Tagalog",
      "code": "tl",

      "strings": {
        "prompt": "Anong pangalan mo?",
        "greeting": "Kamusta"
      }
    },
  }
  
  const language = ref(Languages.english);
  const count = ref(0)
  
  const guests = ref<Array<string>>([])

  function guestExist(name: string) {
    const index = guests.value.findIndex((guest: string) => guest.toLowerCase() === name.toLowerCase())

    return index >= 0
  }
  
  function addGuest(name: string) {
    guests.value.push(name)
  }
  
  function toggleLanguage() {
    count.value ++;
    if (language.value.code === Languages.english.code) {
      language.value = Languages.tagalog
    } else {
      language.value = Languages.english
    }
  }
  
  return { Languages, language, guests, guestExist, addGuest, toggleLanguage, count }
})
